/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab02;

/**
 *
 * @author HP
 */
public class lab2_3 {
    public static int removeDuplicates(int[] nums){
        int k=1;
        for(int i=1;i<nums.length;i++){
            if(nums[i]!=nums[i-1]){
                k++;
            } 
        }
        for (int i=1;i<nums.length;i++){
            if(nums[i]==nums[i-1]){
                i--;
                for(int j=i+1;j<nums.length-1;j++){
                    nums[j] = nums[j+1];
                    nums[j+1] = nums[i];
                }
            }
        }
        System.out.println("k = "+k);
        System.out.print("{");
        for(int i=0;i<nums.length;i++){
            if(i<k){
                System.out.print(nums[i]);
            }else{
                System.out.print("_");
            }
            if(i<nums.length-1){
                System.out.print(",");
            } 
        }
        System.out.println("}");
        return k;
    }
    

    public static void main(String[] args) {
        int[] nums = {1,1,2};
        removeDuplicates(nums);

        int[] nums2 = {0,0,1,1,1,2,2,3,3,4};
        removeDuplicates(nums2);

        int[] nums3 = {0,0,1,1,1,2,2,3,3,3,3,4,4,4,4,5,5,5,6,6,6,7,7,8};
        removeDuplicates(nums3);
        
        System.out.println("hello world");
    }

}
